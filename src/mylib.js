/**Basic arothmetic operations */
const mylib = {
    /** Multiline arrow function */
    add: function (a, b) {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    /** Singleline arrow function */
    divide: (dividend, divisor) => dividend / divisor,
    /** Regular function */
    multiply:(a, b)=>{
        return a * b;
    }
};

module.exports = mylib;