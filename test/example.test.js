// example.test.js
const expect = require("chai").expect;
const mylib = require("../src/mylib");

describe("Unit testing mylib.js", () => {
  before(() => {
    // initialization
    //create objects
    console.log("Initialising tests.");
  });
  // Add test
  it("Should return 2 when using add function with a=1, b=1", () => {
    // Test
    const result = mylib.add(1, 1); // 1 + 1
    expect(result).to.equal(2); // result expected to equal 2
  });
  // Subtract test
  it("Should return 2 when using subtract function with a=6, b=4", () => {
    // Test
    const result = mylib.subtract(6, 4); // 6 - 4
    expect(result).to.equal(2); // result expected to equal 2
  });
  // multiplication test
  it("Should return 6 when using multiply function with a=3, b=2", () => {
    // Test
    const result = mylib.multiply(3, 2); // 12 / 2
    expect(result).to.equal(6); // result expected to equal 6
  });
  // Division test
  it("Should return 6 when using divide function with a=12, b=2", () => {
    // Test
    const result = mylib.divide(12, 2); // 12 / 2
    expect(result).to.equal(6); // result expected to equal 6
  });
  // Division error test
  it("Result infinity. Dividend or divisor is 0", () => {
    // Test
    const result = mylib.divide(12, 0); // 12 / 0
    expect(result).to.equal(Infinity); // result expected to equal infinity
  });
  after(() => {
    // Cleanup
    console.log("Testing complete");
  });
});
